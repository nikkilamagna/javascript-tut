// Create variables for brevity
const input = document.querySelector('input');
const output = document.querySelector('#count-digit');
const slider = document.querySelector('#interior');

// Set initial value for counter variable. `let` tells us this value can be changed
let counter = 0;



// Sets initial input value
input.value = counter;
output.innerHTML = counter;

// Event function. Need some help with this one. 
/**
 * The `addEventListener` method can be attached to a number of elements.
 * With this one, we are attaching the `change` event to the input box in the HTML
 * file. A `change` event is triggered when the `input` value is modified.
 * See: https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/change_event
 */
input.addEventListener('change', (e) => {
  // Not sure how "value" is working across three files
  // Could you clarify the above question/confusion?
  counter = e.target.value;
  output.innerHTML = counter;

  // Slider is adjusted based on counter
  slider.style.width = `${counter}%`;
});

