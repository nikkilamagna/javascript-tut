# Javascript Tutorial

Repo to hold code while learning/teaching javascript.

## Installation
- Install [Node](https://nodejs.org/en/learn/getting-started/how-to-install-nodejs)
- Run `npm install` from this directory

## Development
- Run `npm run dev` from this directory
- Kill dev environment by `<CTRL-C>`
